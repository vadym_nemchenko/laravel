<?php

use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Log::info('Showing user profile for user: ');
    return view('welcome');
});

Route::prefix('test')->group(function() {
    Route::get('{name}', function ($name) {
        return "Test w $name";
    })->where('name', '[A-Za-z]+');

    Route::get('{name2}', function ($name2) {
        return "Test2 w $name2";
    })->where('name2', '[A-Za-z0-9]+');

    Route::get('{id}', function ($id) {
        return "Test n $id";
    })->where('id', '[0-9]+');
});


Route::resource('photos', 'PhotoController');
